import { Heading } from '@chakra-ui/react';
import Todo from './components/Todo';
import AddTodo from './components/AddTodo';
import { VStack } from '@chakra-ui/react';
import { useState, useEffect } from 'react';
;

function App() {
  const initialTodos = [
    {
      id: 1,
      body: 'TACH1',
    },
    {
      id: 2,
      body: 'TACHE2',
    },
  ];


  const [todos, setTodos] = useState(
    () => JSON.parse(localStorage.getItem('todos')) || []
  );

  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos));
  }, [todos]);

  function deleteTodo(id) {
    const newTodos = todos.filter((todo) => {
      return todo.id !== id;
    });
    setTodos(newTodos);
  }

  function addTodo(todo) {
    setTodos([...todos, todo]);
  }

 

  return (
    <VStack p={4}>
    
      <Heading
        mb='8'
        fontWeight='extrabold'
        size='2xl'
        bgGradient='linear(to-r, pink.500, pink.300, blue.500)'
        bgClip='text'
      >
        Todo Application
      </Heading>
      <Todo todos={todos} deleteTodo={deleteTodo} />
      <AddTodo addTodo={addTodo} />
    </VStack>
  );
}

export default App;
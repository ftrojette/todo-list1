import { Input,Button, useToast} from '@chakra-ui/react';
import { useState } from 'react';
import {nanoid} from 'nanoid';

function AddTodo({ addTodo }) {
  
   const toast = useToast();

  function HandleSubmit(e) {
    e.preventDefault();
    if (!content) {
      toast({
        title: 'pas de contenu!',
        status: 'error',
        duration: 2000,
        isClosable: true,
      });
      return;}
      const todo = {
        id: nanoid(),
        body: content,
      };
  
      addTodo(todo);
      setContent('');
    }
  

   const[content,setContent]= useState('');

  return (
    <form onSubmit={HandleSubmit}>
      <div>
        <Input
          variant='filled' 
          placeholder='Add todo'
          value={content}    
          onChange= {(e) => setContent (e.target.value)} 
        />
        <Button colorScheme='green' textAlign='center' px='100' type='submit' m='20px' width='100'>
          Add Todo
        </Button><br/>
        </div>
    </form>
  );
}

export default AddTodo;